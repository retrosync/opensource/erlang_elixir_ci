# This is a multi-stage Dockerfile. In the first stage we fetch and
# install the `gcsfuse` binary. In the second stage we build the app
# and copy `gcsfuse` from the first stage.

FROM ubuntu:bionic
ENV LANG=C.UTF-8
ENV REPLACE_OS_VARS=true
ENV HOME=/opt/app
ENV MIX_ENV=prod
ENV CHROMEDRIVER_VERSION=83.0.4103.14
ENV PATH="/elixir/bin:${PATH}"
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y apt-transport-https apt-utils
RUN apt-get update && apt-get install libmagic-mgc
RUN apt-get install -y --fix-missing wget curl build-essential git \
 fontconfig libfreetype6 libjpeg-turbo8 libpng16-16 libxrender1 xfonts-75dpi \
 xfonts-base inotify-tools vim nano libwxbase3.0-0v5 libwxgtk3.0-0v5 libsctp1
RUN apt-get clean
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
RUN dpkg -i erlang-solutions_1.0_all.deb
RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_amd64.deb
RUN dpkg -i wkhtmltox_0.12.6-1.bionic_amd64.deb
RUN wget https://packages.erlang-solutions.com/erlang/debian/pool/esl-erlang_22.3.4.9-1~ubuntu~bionic_amd64.deb
RUN dpkg -i esl-erlang_22.3.4.9-1~ubuntu~bionic_amd64.deb
RUN rm *.deb
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update
RUN apt-get install -y  nodejs gettext-base unzip && apt-get clean
RUN wget -q https://github.com/elixir-lang/elixir/releases/download/v1.11.4/Precompiled.zip
RUN unzip Precompiled.zip -d elixir

RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN apt-get update && apt-get install -y google-chrome-stable
RUN wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip
RUN rm -rf /opt/selenium/chromedriver
RUN unzip /tmp/chromedriver_linux64.zip -d /opt/selenium
RUN rm /tmp/chromedriver_linux64.zip
RUN mv /opt/selenium/chromedriver /opt/selenium/chromedriver-${CHROMEDRIVER_VERSION}
RUN chmod 755 /opt/selenium/chromedriver-${CHROMEDRIVER_VERSION}
RUN ln -fs /opt/selenium/chromedriver-${CHROMEDRIVER_VERSION} /usr/bin/chromedriver
